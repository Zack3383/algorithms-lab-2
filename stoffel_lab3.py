import math
from io import StringIO
import time
import numpy as np
from scipy.stats import linregress
import heapq
import random


# source https://bit.ly/38HXSoU
def print_heap(tree, total_width=60, fill=' '):
    """Pretty-print a tree.
    total_width depends on your input size"""
    output = StringIO()
    last_row = -1
    for i, n in enumerate(tree):
        if i:
            row = int(math.floor(math.log(i + 1, 2)))
        else:
            row = 0
        if row != last_row:
            output.write('\n')
        columns = 2 ** row
        col_width = total_width // columns
        output.write(str(n).center(col_width, fill))
        last_row = row
    print(output.getvalue())
    print('-' * total_width)
    return


# Min heapifies n elements in the array
def min_heapify(array, i, n):
    left = 2 * i + 1
    right = 2 * i + 2

    # if the left child is smaller than the root
    if left <= n and array[left] < array[i]:
        smallest = left
    else:
        smallest = i

    # if the right child is smaller than the root
    if right <= n and array[right] < array[smallest]:
        smallest = right

    # if the smallest value is not root
    if smallest != i:
        array[i], array[smallest] = array[smallest], array[i]

        min_heapify(array, smallest, n)

    pass


def build_min_heap(array):
    n = len(array) - 1

    start = n // 2

    for i in range(start, -1, -1):
        min_heapify(array, i, n)


def extract_min(array, n):
    min = array[0]
    array[0] = array[n]
    min_heapify(array, 0, n - 1)
    return min


def k_smallest(array, k):
    # Make a min heap from array O(n)
    build_min_heap(array)

    result: int
    for i in range(k):
        result = extract_min(array, len(array) - 1)
    # Extract k elmeents O(klog(n))

    return result


def benchmark(algorithm, input_list, k):
    start_time = time.perf_counter()
    algorithm(input_list, k);
    end_time = time.perf_counter()
    return end_time - start_time


if __name__ == '__main__':
    arr = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    result = k_smallest(arr, 4)
    print("The kth smallest number is " + str(result))

    print("----------------------------------------Benchmarking---------------------------------------")

    num_elements = [10, 100, 1000, 2000, 5000, 10000]
    best_times = []
    avg_times = []
    worst_times = []

    heap_best = []
    heap_avg = []
    heap_worst = []


    # heapsort algorithm to compare our algorithm to
    def heapsort(iterable, k):
        h = []
        for value in iterable:
            heapq.heappush(h, value)
        res = [heapq.heappop(h) for i in range(len(h))]
        return res[k - 1]


    print("test heapsort\n")
    print(heapsort([3, 5, 25, 4000, 29, 2], 2))
    print("\n")

    for num in num_elements:
        lst = [random.randint(0, 100) for i in range(0, num)]
        # avg case
        middle = round(len(lst) / 2)
        avg_times.append(benchmark(k_smallest, lst, middle))
        heap_avg.append(benchmark(heapsort, lst, middle))
        # best case
        best_times.append(benchmark(k_smallest, lst, 1))
        heap_best.append(benchmark(heapsort, lst, 1))
        # worse case
        end = len(lst)
        worst_times.append(benchmark(k_smallest, lst, end))
        heap_worst.append(benchmark(heapsort, lst, end))

    print(best_times)
    print(avg_times)
    print(worst_times)
    print("\n")
    print(heap_best)
    print(heap_avg)
    print(heap_worst)

    m, b, _, _, _ = linregress(np.log(num_elements), np.log(best_times))
    print('Best Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(avg_times))
    print('Avg Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(worst_times))
    print('Worst Case:' + str(m) + ',' + str(b) + '\n')

    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_best))
    print('Best Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_avg))
    print('Avg Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_worst))
    print('Worst Case:' + str(m) + ',' + str(b) + '\n')
