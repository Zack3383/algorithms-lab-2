import math
import random
import time
import numpy as np
from scipy.stats import linregress
from collections import namedtuple

# Defining the radius of the circle to generate a random point from
RADIUS = 10000

# A namedtuple for coordinates so the points are x and y insteand of 0 and 1
cds = namedtuple('cds', ['x', 'y'])


# takes a list of normal tuples and converts it to a list of coordinates
def list_to_coords(list):
    to_return = []
    for item in list:
        to_return.append(cds(item[0], item[1]))

    return to_return


# Determines if point B is on segment AC
def on_segment(A, B, C):
    if (orientation(A, C, B) == 0):
        if ((B.x <= max(A.x, C.x)) & (B.x >= min(A.x, C.x)) &
                (B.y <= max(A.y, C.y)) & (B.y >= min(A.y, C.y))):
            return True

    return False


def intersect(A, B, C, D):
    return (orientation(A, C, D) != orientation(B, C, D) and orientation(A, B, C) != orientation(A, B, D))


# This function returns true if and only if the query line goes through either p1 or p2
def need_new_line(p1, p2, point, extreme):
    return (on_segment(point, p1, extreme) or on_segment(point, p2, extreme))


# finds orientation of triplet ABC, modified code from
# https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
# in order to deal with colinearity better
def orientation(A, B, C):
    val = (((B.y - A.y) * (C.x - B.x)) - ((B.x - A.x) * (C.y - B.y)))
    if val == 0:
        return 0  # Colinear
    if val > 0:
        return 1  # Clockwise
    else:
        return 2  # Counterclockwise


# Returns true if the point p lies
# inside the polygon[] with n vertices
def is_inside_polygon(points, p):
    n = len(points)

    # There must be at least 3 vertices
    # in polygon
    if n < 3:
        return False

    # checks for the special case if the checked point is one of the vertexes
    # causes an infinite loop if not checked here
    for i in range(n):
        if (p == points[i]):
            return True

    # Create a point for line segment from p to "infinite"
    angle = random.random() * 2 * math.pi
    new_angle = (angle + (math.pi / 2)) % (2 * math.pi)

    extreme = cds(RADIUS * math.cos(angle) + p.x, RADIUS * math.sin(angle) + p.y)
    new_line_generated = False

    # ensuring the line doesn't go through any of the points
    i = 0
    while (i < n):
        next = (i + 1) % n
        if (need_new_line(points[i], points[next], p, extreme)):
            print("need new line?")
            # If the current line goes through a point on the shape, use the other angle. If both go through points,
            # then take the bisection of the two. This process will repeat until there is a line that doesn't go
            # through a point, although it is extremely unlikely that this is needed in the first place
            if (new_line_generated):
                new_angle = (angle + new_angle) / 2

            new_line_generated = True
            extreme = cds(RADIUS * math.cos(new_angle) + p.x, RADIUS * math.sin(new_angle) + p.y)
            i = 0
        else:
            i += 1

    count = 0
    for i in range(n):
        next = (i + 1) % n

        # If the two lines intersect, then the code first checks if the point is on a line segment,
        #  then adds 1 to the count of intersections if it isn't. If the point is indeed on a line
        # segment, then the methd automatically returns true
        if (intersect(points[i], points[next], p, extreme)):

            # if point p is on a line segment of the shape, the function will return true
            if on_segment(points[i], p, points[next]):
                return True

            count += 1

    # Return true if count is odd, false otherwise
    return (count % 2 == 1)


# benchmarking function
def benchmark(input_list, x):
    start_time = time.perf_counter()
    is_inside_polygon(input_list, x)
    end_time = time.perf_counter()
    return end_time - start_time


# Driver code
if __name__ == '__main__':
    small_square = list_to_coords([(0, -1), (-2, -1), (-2, 1), (0, 1)])
    p = cds(0, 0)

    print("Yes" if is_inside_polygon(small_square, p) else "No")

    triangle = list_to_coords([(-1, 0), (1, 1), (1, -1)])
    # p is still (0,0)
    print("Yes" if is_inside_polygon(triangle, p) else "No")

    # should be yes
    triangle = list_to_coords([(-1, -1), (1, -1), (0, 1)])
    print("Yes" if is_inside_polygon(triangle, p) else "No")

    # should be No
    p = cds(1, 0.5)
    print("Yes" if not is_inside_polygon(triangle, p) else "No")

    # should be yes
    arch = list_to_coords([(-1, -1), (-1, 1), (-2, 1), (-2, -1), (-3, -1), (-3, 2), (1, 2), (1, -1)])
    p = cds(0, 0)
    print("Yes" if is_inside_polygon(arch, p) else "No")

    # should be yes
    mountain_range = list_to_coords(
        [(-1, 1), (-1, -1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2), (1, 1)])
    print("Yes" if is_inside_polygon(mountain_range, p) else "No")

    # should be yes
    p = cds(0, -0.5)
    print("Yes" if is_inside_polygon(mountain_range, p) else "No")

    # Should be No
    p = cds(-1.2, -0.5)
    print("Yes" if (not is_inside_polygon(mountain_range, p)) else "No")

    print("----------------------Benchmarking------------------------")

    all_poly = {"poly1": [(-1, 0), (1, 1), (1, -1)], "poly2": [(-1, -1), (1, -1), (0, 1)],
                "poly3": [(-1, -1), (1, -1), (0, 1)], "poly4": [(0, -1), (-2, -1), (-2, 1), (0, 1)],
                "poly5": [(-1, -1), (-1, 1), (-2, 1), (-2, -1), (-3, -1), (-3, 2), (1, 2), (1, -1)],
                "poly6": [(-1, 1), (-1, -1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2),
                          (1, 1)],
                "poly7": [(-1, 1), (-1, -1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2),
                          (1, 1)],
                "poly8": [(-1, 1), (-1, -1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2),
                          (1, 1)]}
    points = {"poly1": cds(0, 0), "poly2": cds(0, 0), "poly3": cds(1, 0.5), "poly4": cds(0, 0), "poly5": cds(0, 0),
              "poly6": cds(0, 0), "poly7": cds(-1.2, 0), "poly8": cds(-1.2, -0.5)}

    times = {}

    for key, poly in all_poly.items():
        times[key] = benchmark(list_to_coords(poly), points[key])

    print(times)

    # initializing range
    # i, j = 0, 2
    #
    # n, m = 3, 4
    #
    # x, y = 5, 7
    #
    # # using dictionary comprehension to compile result in one
    # best = {key: val for key, val in filter(lambda sub: int(sub[1]) >= i and int(sub[1]) <= j, times.items())}
    # avg = {key: val for key, val in filter(lambda sub: int(sub[1]) >= n and int(sub[1]) <= m, times.items())}
    # worst = {key: val for key, val in filter(lambda sub: int(sub[1]) >= x and int(sub[1]) <= y, times.items())}

    # Insertion times regression (from best case to worst case)
    # m, b, _, _, _ = linregress(np.log(all_poly.keys), np.log(best))
    # print('Best Case:' + str(m) + '\n')
    # m, b, _, _, _ = linregress(np.log(all_poly.keys), np.log(avg))
    # print('Avg Case:' + str(m) + '\n')
    # m, b, _, _, _ = linregress(np.log(all_poly.keys), np.log(worst))
    # print('Worst Case:' + str(m) + '\n')

