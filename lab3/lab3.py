import math
import time
import random
import numpy as np
from scipy.stats import linregress

def swap(array, a, b):
    temp = array[a]
    array[a] = array[b]
    array[b] = temp


# Retrives the index of the parent node
def get_parent_index(i):
    # 0  1 2  3 4 5 6  7 8 9 10 11 12 13 14
    return math.floor((i - 1) / 2)


# Retrives the index of the left child node
def get_left_child_index(i):
    # 0  1 2  3 4 5 6  7 8 9 10 11 12 13 14
    return math.floor((i * 2) + 1)


# Retrives the index of the left child node
def get_right_child_index(i):
    # 0  1 2  3 4 5 6  7 8 9 10 11 12 13 14
    return math.floor((i * 2) + 2)


def sift_down(heap, i, n):
    left_index = get_left_child_index(i)
    right_index = get_right_child_index(i)

    node_value = heap[i]

    # Check if no child exists
    if left_index >= n:
        return

    # Check if just the left child exists
    elif right_index >= n:
        left_child = heap[left_index]

        if node_value > left_child:
            # Swap with the left child
            swap(heap, i, left_index)
            sift_down(heap, left_index, n)

    # Check if both children exist
    else:
        left_child = heap[left_index]
        right_child = heap[right_index]

        # Check if sifting is needed
        if node_value > left_child or node_value > right_child:

            if left_child < right_child:
                # Swap with the left child
                swap(heap, i, left_index)
                sift_down(heap, left_index, n)

            else:
                # Swap with the right child
                swap(heap, i, right_index)
                sift_down(heap, right_index, n)


# Insert element into heap
def build_min_heap(heap):
    n = len(heap)
    for i in range(len(heap) // 2, -1, -1):
        sift_down(heap, i, n)


# Extract top element from heap
def extract_head(heap, n):
    head = heap[0]
    swap(heap, 0, n - 1)
    sift_down(heap, 0, n - 1)
    return head


def k_smallest(array, k):
    if k <= 0:
        raise "k must be greater than 0"
    if k > len(array):
        raise f"k is out of range of array of length"

    heap = array

    # Make a min heap from array O(n)
    build_min_heap(heap)

    # Extract k elmeents O(klog(n))
    heap_size = len(array)
    for i in range(k - 1):
        extract_head(heap, heap_size)
        heap_size -= 1

    return heap[0]


def benchmark(algorithm, input_list, k):
    start_time = time.perf_counter()
    algorithm(input_list, k);
    end_time = time.perf_counter()
    return end_time - start_time


if __name__ == '__main__':
    arr = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    result = k_smallest(arr, 4)
    print("The kth smallest number is " + str(result))

    num_elements = [10, 100, 1000, 2000, 5000, 10000]
    best_times = []
    avg_times = []
    worst_times = []

    heap_best = []
    heap_avg = []
    heap_worst = []

    print("----------------------------------------Benchmarking---------------------------------------")


    # implementation taken from https://www.geeksforgeeks.org/python-program-for-heap-sort/
    # heapsort algorithm to compare our own algorithm to
    def heapify(arr, n, i):
        largest = i  # Initialize largest as root
        left = 2 * i + 1  # left = 2*i + 1
        right = 2 * i + 2  # right = 2*i + 2

        # See if left child of root exists and is
        # greater than root
        if left < n and arr[i] < arr[left]:
            largest = left

        # See if right child of root exists and is
        # greater than root
        if right < n and arr[largest] < arr[right]:
            largest = right

        # Change root, if needed
        if largest != i:
            arr[i], arr[largest] = arr[largest], arr[i]  # swap

            # Heapify the root.
            heapify(arr, n, largest)


    # The main function to sort an array of given size
    def heapsort(arr, k):
        n = len(arr)

        # Build a maxheap.
        # Since last parent will be at ((n//2)-1) we can start at that location.
        for i in range(n // 2 - 1, -1, -1):
            heapify(arr, n, i)

        # One by one extract elements
        for i in range(n - 1, 0, -1):
            arr[i], arr[0] = arr[0], arr[i]  # swap
            heapify(arr, i, 0)
        return arr[k-1]

    print("test heapsort\n")
    print(heapsort([3, 5, 25, 4000, 29, 2], 2))
    print("\n")

    for num in num_elements:
        lst = [random.randint(0, 100) for i in range(0, num)]
        # avg case
        middle = round(len(lst) / 2)
        avg_times.append(benchmark(k_smallest, lst, middle))
        heap_avg.append(benchmark(heapsort, lst, middle))
        # best case
        best_times.append(benchmark(k_smallest, lst, 1))
        heap_best.append(benchmark(heapsort, lst, 1))
        # worse case
        end = len(lst)
        worst_times.append(benchmark(k_smallest, lst, end))
        heap_worst.append(benchmark(heapsort, lst, end))

    print(best_times)
    print(avg_times)
    print(worst_times)
    print("\n")
    print(heap_best)
    print(heap_avg)
    print(heap_worst)

    m, b, _, _, _ = linregress(np.log(num_elements), np.log(best_times))
    print('Best Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(avg_times))
    print('Avg Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(worst_times))
    print('Worst Case:' + str(m) + ',' + str(b) + '\n')

    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_best))
    print('Best Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_avg))
    print('Avg Case:' + str(m) + ',' + str(b) + '\n')
    m, b, _, _, _ = linregress(np.log(num_elements), np.log(heap_worst))
    print('Worst Case:' + str(m) + ',' + str(b) + '\n')
