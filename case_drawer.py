from turtle import color
import matplotlib.pyplot as plt
from matplotlib import collections  as mc

def drawProblem(shapePts, pt):

    MARGIN = 0.1

    lines = []
    colors = []
    min_x = float("inf")
    max_x = float("-inf")

    for i in range(len(shapePts)):
        pt_a = shapePts[i]
        pt_b = shapePts[i - 1]

        if pt_a[0] < min_x:
            min_x = pt_a[0]

        if pt_a[0] > max_x:
            max_x = pt_a[0]

        lines.append([pt_a, pt_b])
        colors.append("blue")

    # Add ray
    lines.append([pt, (min_x - 1000, pt[1])])
    colors.append("red")

    lc = mc.LineCollection(lines, color=colors, linewidths=2)
    fig, ax = plt.subplots()
    ax.add_collection(lc)
    ax.scatter(pt[0], pt[1], c="red")
    ax.autoscale()
    ax.set_xlim([min_x - MARGIN, max_x + MARGIN])
    ax.margins(MARGIN)
    plt.show()


drawProblem([(0, -1),(-2,-1), (-2, 1), (0, 1)], (0,0))
drawProblem([(-1, 0),(1, 1), (1,-1)], (0,0))
drawProblem([(-1,-1),(1,-1), (0, 1)], (0,0))
drawProblem([(-1,-1),(1,-1), (0, 1)], (1,0.5))
drawProblem([(-1,-1),(-1,1), (-2, 1), (-2, -1), (-3, -1), (-3, 2), (1, 2), (1, -1)], (0,0))
drawProblem([(-1, 1),(-1,-1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2), (1, 1)], (0,0))
drawProblem([(-1, 1),(-1,-1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2), (1, 1)], (0,-0.5))
drawProblem([(-1, 1),(-1,-1), (-2, 0), (-3, -1), (-4, 0), (-5, -1), (-6, 0), (-7, -1), (-7, -2), (1, -2), (1, 1)], (-1.2,-0.5))