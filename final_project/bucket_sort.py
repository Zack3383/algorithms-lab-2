import numpy as np
import time

def bucket_sort(lst, k):
    buckets = make_buckets(k)

    max = lst[0]
    min = lst[-1]
    for item in lst:
        if item > max:
            max = item
        if item < min:
            min = item

    bucket_range = (max - min)/k

    for item in lst:
        index = int((item - min) / bucket_range)
        if index > k - 1:
            index = -1
        buckets[index].append(item)

    for i in range(k):
        buckets[i] = insertion_sort(buckets[i])

    ret = []
    for i in range(k):
        for j in range(len(buckets[i])):
            ret.append(buckets[i][j])
    return ret


def insertion_sort(lst):
    for i in range(1, len(lst)):
        key = lst[i]
        j = i - 1
        while j >= 0 and lst[j] > key:
            lst[j+1] = lst[j]
            j -= 1
            
        lst[j+1] = key

    return lst

def make_buckets(k):
    bucket_list = []
    for i in range(k):
        bucket_list.append([])
    return bucket_list

# list1 = [16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1]
# print(bucket_sort(list1, 16))
